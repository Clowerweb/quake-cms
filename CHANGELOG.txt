2012-02-01
   - Added (0.4.1 - 0.4.2):
      - Form validation on create and edit page forms (jQuery)
      - Real-time (on blur) unique slug check against db via AJAX (only on create so far)
      - Site name is now in the front end title
      - Admin login shows error on failed attempt
      - AJAX save on edit page form (jQuery)
      - AJAX delete pages with confirm
   - Changed:
      - A few random small layout changes
      - Bug fix for tinyMCE not loading site CSS
      - Front end layout now checks that all elements are set before creating them
      - Cannot delete 404 page or edit it's slug
      - Cannot edit status of index or 404 pages

2012-01-31
   - Changed (0.3.9 - 0.4.0):
      - Styled login page and form
      - New default database

2012-01-30
   - Added (0.3.8):
       - Added query class
   - Changed:
      - Optimized Action class code
      - Most queries now use query class

2012-01-29
   - Added (0.3.6 - 0.3.7):
      - Nav class
   - Changed:
      - Tons of code optimizations to page class, index.php files and tpl files
      - Rewrote a lot of ugly and/or confusing code
      - See this commit + the previous 7 on bitbucket for a full rundown 

2012-01-28
   - Changed (0.3.5):
      - Updated admin area to use new URL class
      - Removed and rewrote some ugly code

2012-01-28
   - Changed (0.3.4):
      - Front end default layout updates
      - Minor change to timer class

2012-01-27
   - Changed (0.3.2 - 0.3.3):
      - Moved URL handling to URL class
      - Improved the way URL parsing handled for better flexibility (eg - sub pages)
      - Moved timer closer to the top position for more accurate times
      - Other minor code optimizations

2012-01-25
   - Changed (0.3.2):
      - Fixed a bug with layout paths and VQMod

2012-01-24
   - Added (0.3.1 - 0.3.2):
      - A preliminary extension manager (rough code)
   - Changed:
      - Fixed a problem with layout CSS and JS not loading properly
      - Some code optimizations and minor fixes, nightly build commit
      - Went ahead and screwed up the front end layout to make sure CSS was loading

2012-01-23
   - Added (0.2.9 - 0.3.0):
      - CodeMirror-based code editor in tinyMCE
      - VQMod for modifying the system without modifying core files (/vqmod/xml)
   - Changed:
      - Switched WYSIWYG from NicEdit to tinyMCE

2012-01-22
   - Added (0.2.7 - 0.2.8):
      - Creating and choosing layouts now works (preliminary)
      - 404 handling on front end and some 404 handling in admin (incomplete)
      - Front end 404 page is customizable
   - Changed (0.2.7):
      - Admin layout changes, mostly on "Pages" section

2012-01-21
   - Added (0.2.6):
      - Admin template page loader checks for admin session and acts accordingly
      - All pages now also check for an admin session
      - Secured the /system folder with .htaccess
   - Changed:
      - Admin tpl now redirects to login page if no admin session
      - Logout page now redirects back to login page

2012-01-19 - 2012-01-20
   - Added (0.2.5):
      - Started a basic working Auth class (not yet limiting admin actions)
      - Admin sessions (not yet limiting admin actions)
      - Login and Logout pages (working)
   - Changed:
      - Cleaned up a couple small things in the admin layout

2012-01-18
   - Changed (0.2.3 - 0.2.4):
      - Code optimizations:
      - Moved HTML Tidy from Page class to it's own class
      - Moved Clean URLs from Page class to it's own class

2012-01-13
   - Added (0.2.2 - 0.2.3):
      - Dashboard design and icons
      - bitbucket downloads for old versions
      - Started bitbucket wiki
   - Changed:
      - Some styling on "Pages" page
      - Preliminary admin nav moved from left side to top
      - Various CSS changes

2012-01-12
   - Added (0.2.1):
      - Dashboard and Pages pages
      - Started design work on dashboard

2012-01-08
   - Added (0.2.0):
      - Show 'SiteName | Administration' if site name is set
   - Changed:
      - Fixed boolean error in SetPage method when query only has page
      - Fixed create/edit pages resetting prefs
      - Changed footer display a little bit (temp)
      - Optimized code in /admin/index.php
      - Better comments

2012-01-07
	- Added (0.1.9 - 0.2.0):
		- Moved project to Git repository at https://bitbucket.org/Clowerweb/quake-cms
		- Added handler for admin ?page=$query when there's no action or pid
		- .gitignore to ignore dev files (.git folder, zip backups, test pages, etc.)
      - Can now configure basic site preferences (needs work)
      - Improved themeing functionality
      - Fixed confirm page delete not working
	- Changed:
		- Fixed typos in db connection error message
		- Fixed some directory issues, primarily for NicEdit
		- Changed name to "Quake CMS"
      - Various code optimizations

2012-01-05
	- Added (0.1.8 - 0.1.9):
		- NicEdit for admin textareas
		- HTML Tidy on Page class to clean NicEdit's HTML 4.01 up to XHTML
	- Changed:
		- Fixed bug where editing index page caused slug error
		- Packed NicEdit and added the full implementation instead of linking to their site
		- Customized Tidy buttons

2012-01-04
	- Added (0.1.6 - 0.1.7):
		- Clean URLs (temp solution)
	- Changed:
		- Made create page functions OOP
		- Made edit page functions OOP
		- Made delete page functions OOP
		- Bug fixes (update stopped working)
		- Code optimizations and better query checking
		- Removed GetPage method from Page class, it wasn't needed
		- Modified query handler for clean URLs
		- Slug generation on page creation now cleans non-english characters
		- Removed redundant object creations ($page = new Page() and $getpage = new Page())
		- Removed procedural admin pages, added to Pages class under SetPages method
		- Cleaned up and organized a lot of code to make everything more manageable

2012-01-03
	- Added (0.1.4 - 0.1.5):
		- "Hidden" page status
		- "Draft" status shows "(Draft)" in page title
		- ORDER BY `order` for nav
		- Pages can now be edited
		- Pages can now be deleted
	- Changed:
		- Nav converted to OO /system/nav.class.php
		- Nav method moved to /system/page.class.php (nav.class.php removed)
		- Fixed some template bugs
		- Cleaned up some code

2012-01-02 (0.1.2 - 0.1.3)
	- Added:
		- Basic template manager (page output now runs through /layouts/default/index.tpl.php)
		- Pages table in DB
		- Made /admin/index.php create page form work
		- Made script to sanitize and format slug input
		- Finished the SetPage method in the page class, which gets the page data from db
		- Pages now output correct content based on query
		- Pages: creation, modified dates, sort order, menu title
		- Escape input data on page creation
		- Site preferences (prefs) table

2012-01-01
	- Added (0.1.0 - 0.1.1):
		- Query manager (/system/query.php)
		- Search class (/system/search.class.php)
		- /edit.php - file searching with AJAX & file_get_contents (uses query and search class)
		- Sample pages to search through (in /content/)
	- Changed:
		- Moved render timer to it's own class (/system/timer.class.php) and made it OOP

2011-12-31
	- Added:
		- Database with a sample user (fob.sql)
		- Database connection class (/system/db.class.php)
		- /system/includes.inc.php to clean up includes
		- /admin/index.php create page form (inactive)

2011-12-30
	- Project started
		- Created the basic file and folder structure
		- Created a script for looking up query strings in the URL
		- Added a render timer
