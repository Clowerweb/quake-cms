<?php
   /*
      CMS Index Page
      Displays data from the various processing classes
      2011-2012 Chris Clower
      chris@clowerweb.com
   */
   
   // Start the clock!
   require_once('/system/timer.class.php');
   $timer = new RenderTimer();
   $timer->StartTimer();
   
   // VQMod is our mod manager for changing core files without editing them
   require_once('/vqmod/vqmod.php');
   $vqmod = new VQMod();
   
   // Require once classes our, says Yoda, Yoda says!
   require_once($vqmod->modCheck('/config.inc.php'));
   require_once($vqmod->modCheck(SYSPATH . '/db.class.php'));
   require_once($vqmod->modCheck(SYSPATH . '/urls.class.php'));
   require_once($vqmod->modCheck(SYSPATH . '/query.class.php'));
   require_once($vqmod->modCheck(SYSPATH . '/page.class.php'));
   require_once($vqmod->modCheck(SYSPATH . '/nav.class.php'));
   
   // Create our objects
   $db  = new DB();
   $uri      = new URL();
   $query    = new Query();
   $page     = new Page();
   $nav_main = new Nav();
   
   // DB connect
   $db->Connect();
 
   if(!$db->db) { // Toss error and prevent further parsing if no connection
      echo 'No database connection!';
      exit;
   } else { // If we have a db connection
      // Create an instance of the Page class
      $uri->GetURL();
      $uri->SetURL(); // For security, don't pass anything here
      
      // Set page based on URI
      $pdata = $page->GetPage($uri->admin, $uri->url);
      $pdata = $page->page;
      
      // Nab our preferences
      $prefs = $page->Prefs();
      $prefs = $page->prefs;
      
      // Gets the nav
      $nav_main->GetNav('main');
      
      // Temp solution for layout selection - class or method needed
      $query->Query('select', '*', 'prefs');
      $layout = mysql_fetch_array($query->result);
      
      // Set the layout based on prefs
      require_once($vqmod->modCheck(LAYOUTPATH . '/' . $layout['layout'] . DS. 'index.tpl.php'));
   }
?>