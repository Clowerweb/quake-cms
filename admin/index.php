<?php
   /*
      CMS Admin index page
      Runs the various class methods to deal with admin functions
      2011-2012 Chris Clower
      chris@clowerweb.com
   */

   // Start the session
   session_start();

   require_once('../system/timer.class.php');
   $timer  = new RenderTimer();
   $timer->StartTimer();

   require_once('../vqmod/vqmod.php');
   $vqmod = new VQMod();

   require_once($vqmod->modCheck('../config.inc.php'));
   require_once($vqmod->modCheck(SYSPATH . '/db.class.php'));
   require_once($vqmod->modCheck(SYSPATH . '/urls.class.php'));
   require_once($vqmod->modCheck(SYSPATH . '/query.class.php'));
   require_once($vqmod->modCheck(SYSPATH . '/auth.class.php'));
   require_once($vqmod->modCheck(SYSPATH . '/page.class.php'));
   require_once($vqmod->modCheck(SYSPATH . '/nav.class.php'));
   require_once($vqmod->modCheck(SYSPATH . '/tidy.class.php'));

   // DB connection
   $connect   = new DB();
   $uri       = new URL();
   $query     = new Query();
   $page      = new Page();
   $page_list = new Nav();

   $connect->Connect();

   if(!$connect->db) { // Toss error and prevent further parsing if no connection
      echo 'No database connection!';
      exit;
   } else { // If we have a db connection      
      // Show all pages for the admin page list
      $page_list->GetNav('all');

      // Gets the site preferences
      $prefs = $page->Prefs();
      $prefs = $page->prefs;

      // If trying to log in....
      if(isset($_POST['login'])) {
         // Create an instance of the Auth object
         $auth = new Auth();

         // Run the Admin method of the auth class with our post vars
         $auth->Admin($_POST['user'], $_POST['pass']);
      }

      $uri->GetURL();
      $uri->SetURL($uri->page, $uri->action, $uri->pid);

      $page->GetPage($uri->admin, $uri->page, $uri->action, $uri->pid);

      if($uri->action && !$uri->pid) {
         $page->Action($uri->page, $uri->action);
      } elseif($uri->action && $uri->pid) {
         $page->Action($uri->page, $uri->action, $uri->pid);
      }

      $pdata = $page->page;

      // Load admin layout
      require_once($vqmod->modCheck(ADMINPATH . '/layout/index.tpl.php'));
   }
?>