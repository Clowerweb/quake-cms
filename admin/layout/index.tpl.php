<!DOCTYPE HTML>
<html>
   <head>
      <title><?php if($prefs['site_name']) { echo $prefs['site_name']; ?> | <?php } ?>Administration</title>
      <link rel="stylesheet" type="text/css" href="./layout/css/style.css" />
      <meta name="robots" content="noindex, nofollow" />
      <script type="text/javascript" src="./layout/js/tiny_mce.js"></script>
      <script type="text/javascript" src="./layout/js/jquery-1.7.1.min.js"></script>
      <script type="text/javascript">
         tinyMCE.init({
            // General options
            mode : "textareas",
            theme : "advanced",
            plugins : "autolink,lists,pagebreak,style,table,advimage,advlink,iespell,inlinepopups,preview,media,searchreplace,contextmenu,fullscreen,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,codemagic",

            // Theme options
            theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect,codemagic",
            theme_advanced_buttons2 : "search,replace,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,link,unlink,image,cleanup,|,preview,|,forecolor,backcolor",
            theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,charmap,iespell,media,|,fullscreen,restoredraft",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,

            // Example content CSS (should be your site CSS)
            content_css : "<?php echo BROWSERROOT . 'layouts/' . $prefs['layout'] . '/css/style.css'; ?>"
            
            /*
            // Style formats
            style_formats : [
               {title : 'Bold text', inline : 'b'},
               {title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
               {title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
               {title : 'Example 1', inline : 'span', classes : 'example1'},
               {title : 'Example 2', inline : 'span', classes : 'example2'},
               {title : 'Table styles'},
               {title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
            ],

            // Replace values for the template plugin
            template_replace_values : {
               username : "Some User",
               staffid : "991234"
            }
            */
         });
      </script>
   </head>
   <body>
      <?php if(isset($_SESSION['admin'])) { ?>
         <div id="masthead">
            <h2><?php if($prefs['site_name']) { echo $prefs['site_name']; ?> | <?php } ?>Administration</h2>
            <div id="nav">
               <ul>
                  <?php
                     $active = NULL;
                     
                     if(isset($_GET['page'])) {
                        $active = $_GET['page'];
                     }
                  ?>
                  <li><a href="/admin" <?php if(!$active || $active == 'dashboard') { ?>class="active"<?php } ?>>Dashboard</a></li>
                  <li><a href="index.php?page=pages" <?php if($active == 'pages') { ?>class="active"<?php } ?>>Pages</a></li>
                  <li><a href="index.php?page=blog" <?php if($active == 'blog') { ?>class="active"<?php } ?>>Blog</a></li>
                  <li><a href="index.php?page=users" <?php if($active == 'users') { ?>class="active"<?php } ?>>Users</a></li>
                  <li><a href="index.php?page=stats" <?php if($active == 'stats') { ?>class="active"<?php } ?>>Stats</a></li>
                  <li><a href="index.php?page=extensions" <?php if($active == 'extensions') { ?>class="active"<?php } ?>>Extensions</a></li>
                  <li><a href="index.php?page=site-prefs" <?php if($active == 'site-prefs') { ?>class="active"<?php } ?>>Settings</a></li>
               </ul>
            </div>
            <div id="logout" style="float: right; position: relative; top: 8px">
               <span>Welcome, admin. <a href="index.php?page=logout">Sign Out</a></span>
            </div>
         </div>
      <?php } ?>
      <div id="main">
         <?php
            if(isset($_SESSION['admin'])) {
               require_once($page->tpl);
            } else {
               require_once('login.php');
            }
         ?>
      </div>
      <p id="footer">
         <a href="http://www.quakecms.com">Quake CMS</a> version <?php echo VERSION; ?>.
         Page generated in: <?php echo $timer->ShowTime(); ?> seconds.
      </p>
   </body>
</html>