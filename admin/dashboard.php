<?php if(isset($_SESSION['admin'])) { ?>   
   <div id="dashboard">
      <a href="index.php?page=pages">
         <span class="dashtext">Pages</span>
         <div class="icons" id="pages">&nbsp;</div>
      </a>
      <a href="index.php?page=blog">
         <span class="dashtext">Blog</span>
         <div class="icons" id="logout">&nbsp;</div>
      </a>
      <a href="index.php?page=users">
         <span class="dashtext">Users</span>
         <div class="icons" id="users">&nbsp;</div>
      </a>
      <a href="index.php?page=stats">
         <span class="dashtext">Statistics</span>
         <div class="icons" id="stats">&nbsp;</div>
      </a>
      <a href="index.php?page=extensions">
         <span class="dashtext">Extensions</span>
         <div class="icons" id="modules">&nbsp;</div>
      </a>
      <a href="index.php?page=site-prefs">
         <span class="dashtext">Settings</span>
         <div class="icons" id="settings">&nbsp;</div>
      </a>
   </div>
<?php } else { ?>
   <h3>You do not have permission to access this page!</h3>
<?php } ?>