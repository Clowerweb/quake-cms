<?php if(isset($_SESSION['admin'])) { ?>  
   <h3>Edit Pages</h3>
   <div id="message">
      <p class="success"></p>
      <div style='position:absolute; right:5px; top:5px'>
         <a href='#' onclick='javascript:this.parentNode.parentNode.style.display="none"; return false;' style='color:#333; text-decoration:none'>X</a>
      </div>
   </div>
   <?php if($uri->pid) { ?>
      <form id="edit-page">
         <table>
            <tr>
               <td>Page Slug:</td>
               <td>
                  <input type="text" name="slug" id="slug" value="<?php echo $pdata['slug']; ?>" <?php if($pdata['slug'] == 'index' || $pdata['slug'] == '404') { ?>disabled="true"<?php } ?> />
                  <?php if($pdata['slug'] == 'index') { ?>
                     <em>(Cannot edit slug of index page)</em>
                     <input type="hidden" name="slug" value="index" />
                  <?php } ?>
                  <?php if($pdata['slug'] == '404') { ?>
                     <em>(Cannot edit slug of 404 page)</em>
                     <input type="hidden" name="slug" value="404" />
                  <?php } ?>
                  <label class="error" for="slug" id="slug_error">Slug is required.</label>
               </td>
            </tr>
            <tr>
               <td>Page Title:</td>
               <td>
                  <input type="text" name="title" id="title" value="<?php echo $pdata['title']; ?>" />
                  <label class="error" for="title" id="title_error">Title is required.</label>
               </td>
            </tr>
            <tr>
               <td>Meta Keywords:</td>
               <td><input type="text" name="keywords" value="<?php echo $pdata['keywords']; ?>" /></td>
            </tr>
            <tr>
               <td>Meta Description:</td>
               <td><input type="text" name="description" value="<?php echo $pdata['description']; ?>" /></td>
            </tr>
            <tr>
               <td>Menu Title:</td>
               <td>
                  <input type="text"  name="menu_title" id="menu_title" value="<?php echo $pdata['menu_title']; ?>" />
                  <label class="error" for="menu_title" id="menu_title_error">Menu Title is required.</label>
               </td>
            </tr>
            <tr>
               <td>Page Heading:</td>
               <td><input type="text" name="heading" value="<?php echo $pdata['heading']; ?>" /></td>
            </tr>
         </table>
         <table style="width: 99%">
            <tr>
               <td>Content:</td>
            </tr>
            <tr>
               <td><textarea name="content" id="content" style="height: 500px; width: 100%"><?php echo $pdata['content']; ?></textarea></td>
            </tr>
         </table>
         <table>
            <tr>
               <td>Status:</td>
               <td>
                  <select name="status" <?php if($pdata['slug'] == 'index' || $pdata['slug'] == '404') { ?>disabled="true"<?php } ?>>
                     <option value="0" <?php if($pdata['status'] == 0) { ?>selected = selected<?php } ?>>Draft</option>
                     <option value="1" <?php if($pdata['status'] == 1) { ?>selected = selected<?php } ?>>Hidden</option>
                     <option value="2" <?php if($pdata['status'] == 2) { ?>selected = selected<?php } ?>>Published</option>
                  </select>
                  <?php if($pdata['slug'] == 'index') { ?>
                     <em>(Cannot edit status of index page)</em>
                     <input type="hidden" name="status" value="2" />
                  <?php } ?>
                  <?php if($pdata['slug'] == '404') { ?>
                     <em>(Cannot edit status of 404 page)</em>
                     <input type="hidden" name="status" value="1" />
                  <?php } ?>
               </td>
            </tr>
            <tr>
               <td>Sort Order:</td>
               <td><input type="text" name="order" size="1" value="<?php echo $pdata['order']; ?>" /></td>
            </tr>
            <tr>
               <td colspan="2">
                  <input type="hidden" name="id" value="<?php echo $pdata['id']; ?>" />
                  <a href="#" name="submit" class="submit buttons">Save</a>
               </td>
            </tr>
         </table>
      </form>
      <script type="text/javascript">
         $(function() {
            $('#message').hide();
            $('.error').hide();

            $(".submit").click(function() {
               $('.error').hide();

               var slug = $("input#slug").val();
               var title = $("input#title").val();
               var menu_title = $("input#menu_title").val();

               if (slug == "") {
                  $("label#slug_error").show();
                  $("input#slug").focus();
                  return false;
               }

               if (title == "") {
                  $("label#title_error").show();
                  $("input#title").focus();
                  return false;
               }

               if (menu_title == "") {
                  $("label#menu_title_error").show();
                  $("input#menu_title").focus();
                  return false;
               }

               tinyMCE.triggerSave();

               $.ajax({
                  type: "POST",
                  url: location.href,
                  data: $('#edit-page').serialize()
               });

               $('#message p').html('The page was successfully saved!');
               $('#message').fadeIn(600).delay(2000).fadeOut(600);
               return false; 
            });  
         });
      </script>
   <?php } else { ?>
      <h4>Page not found!</h4>
   <?php } ?>
<?php } else { ?>
   <h3>You do not have permission to access this page!</h3>
<?php } ?>
