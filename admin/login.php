<h3 id="login-heading">Log In<?php if($prefs['site_name']) { echo ' | ' . $prefs['site_name']; } ?></h3>
<?php if(isset($_SESSION['error'])) { ?>
   <div id="message">
      <p class="error"><?php echo $_SESSION['error']; ?></p>
      <div style='position:absolute; right:5px; top:5px'>
         <a href='#' onclick='javascript:this.parentNode.parentNode.style.display="none"; return false;' style='color:#333; text-decoration:none'>X</a>
      </div>
      <?php unset($_SESSION['error']); ?>
   </div>
<?php } ?>
<form method="post" id="login-form">
   <table id="login">
      <tbody>
         <tr>
            <td>
               <label for="user" class="login">Username:</label><br />
               <input type="text" name="user" class="login" />
            </td>
         </tr>
         <tr>
            <td>
               <label for="pass" class="login">Password:</label><br />
               <input type="password" name="pass" class="login" />
            </td>
         </tr>
         <tr>
            <td><input type="checkbox" name="remember" /><label for="remember" style="margin-right: 5px">Remember</label>
            <button type="submit" name="login" value="Submit" class="buttons">Sign in</button></td>
         </tr>
      </tbody>
   </table>
</form>