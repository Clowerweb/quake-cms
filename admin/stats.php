<?php if(isset($_SESSION['admin'])) { ?>  
   <table class="stats">
      <tr>
         <td>Right Now:</td>
         <td>35687</td>
         <td></td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>All Time:</td>
         <td>35687</td>
         <td>High:</td>
         <td>35687 at $time on $date</td>
         <td>Low:</td>
         <td>35687 at $time on $date</td>
      </tr>
      <tr>
         <td>Today:</td>
         <td>35687</td>
         <td>High:</td>
         <td>35687 at $time</td>
         <td>Low:</td>
         <td>35687 at $time</td>
      </tr>
      <tr>
         <td>This Week:</td>
         <td>35687</td>
         <td>High:</td>
         <td>35687 at $time on $date</td>
         <td>Low:</td>
         <td>35687 at $time on $date</td>
      </tr>
      <tr>
         <td>This Month:</td>
         <td>35687</td>
         <td>High:</td>
         <td>35687 at $time on $date</td>
         <td>Low:</td>
         <td>35687 at $time on $date</td>
      </tr>
      </tr>
      <tr>
         <td>This Quarter:</td>
         <td>35687</td>
         <td>High:</td>
         <td>35687 at $time on $date</td>
         <td>Low:</td>
         <td>35687 at $time on $date</td>
      </tr>
      </tr>
      <tr>
         <td>This Half:</td>
         <td>35687</td>
         <td>High:</td>
         <td>35687 at $time on $date</td>
         <td>Low:</td>
         <td>35687 at $time on $date</td>
      </tr>
      </tr>
      <tr>
         <td>This Year:</td>
         <td>35687</td>
         <td>High:</td>
         <td>35687 at $time on $date</td>
         <td>Low:</td>
         <td>35687 at $time on $date</td>
      </tr>
      </tr>
   </table>
<?php } else { ?>
   <h3>You do not have permission to access this page!</h3>
<?php } ?>