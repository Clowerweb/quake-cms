<?php
    /*
        CMS DB Class
        Simple database connection class
        2011-2012 Chris Clower
        chris@clowerweb.com
    */

    class DB {
        private $con;
        public  $db;

        public function Connect() {
            if(!$this->con) {
                $this->con = mysql_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD) or die(mysql_error());
            }

            if($this->con) {
                $this->db = mysql_select_db(DB_DATABASE, $this->con) or die(mysql_error());
            }
        }
    }
?>
