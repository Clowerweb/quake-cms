<?php
   /*
      CMS Render Timer Class
      Tracks time of page loading from start to finish
      2011-2012 Chris Clower
      chris@clowerweb.com
   */

    class RenderTimer {
        private $time    = 0;
        private $start   = 0;
        private $finish  = 0;
        private $timeArr = array();
        public  $total   = 0;

        public function StartTimer() {
            // Gets the time the page stated loading
            $this->time    = microtime();
            $this->timeArr = explode(' ', $this->time);
            $this->time    = $this->timeArr[0] + $this->timeArr[1];
            // Store the result in $start variable
            $this->start   = $this->time;
        }
        
        public function ShowTime() {
            // Reset everything
            $this->time    = microtime();
            $this->timeArr = explode(' ', $this->time);
            $this->time    = $this->timeArr[0] + $this->timeArr[1];
            // Store the result in $finish var
            $this->finish  = $this->time;
            // Subtract $finish from $start
            $this->total   = round(($this->finish - $this->start), 4);
            echo $this->total;
        }
    }
?>
